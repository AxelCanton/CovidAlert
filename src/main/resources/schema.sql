CREATE TABLE locations(
                          id_location serial PRIMARY KEY,
                          latitude numeric(18, 16) NOT NULL,
                          longitude numeric(18, 16) NOT NULL,
                          date timestamp without time zone NOT NULL,
                          id_user varchar(36) NOT NULL
);