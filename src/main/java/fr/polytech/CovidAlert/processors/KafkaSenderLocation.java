package fr.polytech.CovidAlert.processors;

import fr.polytech.CovidAlert.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaSenderLocation {

    private KafkaTemplate<String, Location> kafkaTemplate;

    @Autowired
    KafkaSenderLocation(KafkaTemplate<String, Location> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(Location location, String topic) {
        this.kafkaTemplate.send(topic, location);
    }
}
