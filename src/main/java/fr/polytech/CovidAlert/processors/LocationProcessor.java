package fr.polytech.CovidAlert.processors;

import fr.polytech.CovidAlert.models.Location;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class LocationProcessor {

    private static LocationProcessor singleton;
    private LinkedList<Location> locations;

    public static LocationProcessor getInstance() {
        if(singleton == null) {
            singleton = new LocationProcessor();
        }
        return singleton;
    }

    private LocationProcessor() {
        this.locations = new LinkedList<>();
    }

    public LinkedList<Location> getLocations() {
        return locations;
    }

    public List<Location> getUserLocations(Long id_user) {
        return locations.stream().filter(location -> String.valueOf(id_user).equals(location.getId_user())).collect(Collectors.toList());
    }

    public void addLocation(Location location) {
        this.locations.add(location);
    }
}
