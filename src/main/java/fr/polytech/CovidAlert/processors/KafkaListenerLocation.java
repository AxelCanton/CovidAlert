package fr.polytech.CovidAlert.processors;

import fr.polytech.CovidAlert.models.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
@KafkaListener(id = "class-level", topics = "UserLocation")
public class KafkaListenerLocation {

    Logger LOG = LoggerFactory.getLogger(KafkaListenerLocation.class);

    private LocationProcessor locationProcessor = LocationProcessor.getInstance();

    @KafkaListener(
            groupId = "location",
            topicPartitions = @TopicPartition(
                    topic = "UserLocation",
                    partitionOffsets = {
                            @PartitionOffset(
                                    partition = "0",
                                    initialOffset = "0"
                            )
                    }
            )
    )
    void listener(
            @Payload Location location,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
            @Header(KafkaHeaders.OFFSET) int offset,
            @Header(name= KafkaHeaders.RECEIVED_MESSAGE_KEY, required = false) Integer key,
            @Header(KafkaHeaders.RECEIVED_TIMESTAMP)long time
    ) {
        this.locationProcessor.addLocation(location);
        Timestamp timestamp = new Timestamp(time);
        LOG.info("Key: " + key + "\nLocation: " + location + "\nTimestamp: " + timestamp);
    }

}
