package fr.polytech.CovidAlert.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicsConfig {

    @Bean
    public NewTopic userLocation(){
        return new NewTopic("UserLocation", 6, (short)3);
    }

    @Bean
    public NewTopic suspiciousLocations(){
        return new NewTopic("SuspiciousLocations", 6, (short)3);
    }
    
}
