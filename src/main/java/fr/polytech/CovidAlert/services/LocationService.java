package fr.polytech.CovidAlert.services;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import fr.polytech.CovidAlert.models.Location;
import fr.polytech.CovidAlert.repositories.LocationRepository;

@Service
@Component
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public Optional<Location> get(Long id) {
        return locationRepository.findById(id);
    }

    public Location create(Location loc) {
        return locationRepository.saveAndFlush(loc);
    }

    public void delete(Long id) throws NoSuchElementException {
        if(locationRepository.existsById(id)){
            locationRepository.deleteById(id);
        } else {
            throw new NoSuchElementException();
        }
    }
}
