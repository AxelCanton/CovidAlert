package fr.polytech.CovidAlert.controllers;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import fr.polytech.CovidAlert.processors.KafkaSenderLocation;
import fr.polytech.CovidAlert.processors.LocationProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.polytech.CovidAlert.models.Location;
import fr.polytech.CovidAlert.services.LocationService;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/locations")
public class LocationsController {

    @Autowired
    private LocationService locationService;

    @Autowired
    KafkaSenderLocation kafkaSender;

    private LocationProcessor locationProcessor = LocationProcessor.getInstance();

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getLocation(@PathVariable Long id) {
        Optional<Location> optionalLocation = locationService.get(id);
        if(optionalLocation.isPresent()) {
            return ResponseEntity.ok(optionalLocation.get());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping @RequestMapping("/user/{id_user}")
    public List<Location> getLocationByUser(@PathVariable Long id_user) {

        return locationProcessor.getUserLocations(id_user);

    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody final Location loc) {
        kafkaSender.sendMessage(loc, "UserLocation");
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        try {
            locationService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
