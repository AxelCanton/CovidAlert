package fr.polytech.CovidAlert;

import fr.polytech.CovidAlert.models.Location;
import fr.polytech.CovidAlert.repositories.LocationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LocationRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private LocationRepository repository;

    @Test
    @DisplayName("FindById - Found")
    void testFindByIdFound() {
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);
        Optional<Location> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertTrue(foundTest.isPresent(), "Location was not found");
        Assertions.assertSame(foundTest.get(), mockLocation,  "Location found was not the same");
    }

    @Test
    @DisplayName("FindById - Not found")
    void testFindByIdNotFound() {
        // Data persists through every tests, so to ensure that a data with the provided id is not found,
        // we look for a data that has just been removed
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);
        entityManager.remove(mockLocation);

        Optional<Location> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertFalse(foundTest.isPresent(), "Location was found");
    }

    @Test
    @DisplayName("SaveAndFlush - Success")
    public void testSaveAndFlushSuccess() {
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Location createdTest = repository.saveAndFlush(mockLocation);

        // Assertions
        Assertions.assertSame(createdTest, mockLocation,"Location was not the same");
        entityManager.remove(mockLocation);
    }

    @Test
    @DisplayName("ExistsById - Found")
    public void existsByIdFound() {
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertTrue(isFound, "Location was not found");
    }

    @Test
    @DisplayName("ExistsById - Not found")
    public void existsByIdNotFound() {
        // Same than FindById not found
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);
        entityManager.remove(mockLocation);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertFalse(isFound, "Location was not found");
    }

    @Test
    @DisplayName("ExistsById - Provided id null")
    public void existsByIdThrowException() {
        // Assertions
        Assertions.assertThrows(InvalidDataAccessApiUsageException.class, () -> repository.existsById(null));
    }

    @Test
    @DisplayName("DeleteById - Success")
    public void deleteByIdSuccess() {
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);
        repository.deleteById(id);
    }

    @Test
    @DisplayName("DeleteById - Not found")
    public void deleteByIdNotFound() {
        Location mockLocation = new Location( 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockLocation);
        entityManager.remove(mockLocation);

        // Assertions
        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> repository.deleteById(id));
    }
}
