package fr.polytech.CovidAlert;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.polytech.CovidAlert.controllers.LocationsController;
import fr.polytech.CovidAlert.models.Location;
import fr.polytech.CovidAlert.processors.KafkaSenderLocation;
import fr.polytech.CovidAlert.services.LocationService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(LocationsController.class)
public class LocationControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KafkaSenderLocation kafkaSender;

    @MockBean
    private LocationService locationService;

    private String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("GET /locations/1 - Found")
    public void getFound() throws Exception {
        Location mockLocation = new Location(1, 13.0, 12.0,  new Timestamp(1000000), "a34kAo90");
        doReturn(Optional.of(mockLocation)).when(locationService).get(1L);
        mockMvc.perform(get("/locations/1", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id_location", is(1)))
                .andExpect(jsonPath("$.latitude", is(13.0)))
                .andExpect(jsonPath("$.longitude", is(12.0)))
                .andExpect(jsonPath("$.id_user", is("a34kAo90")));

    }


    @Test
    @DisplayName("GET /locations/1 - Not Found")
    public void getNotFound() throws Exception {
        doReturn(Optional.empty()).when(locationService).get(1L);

        mockMvc.perform(get("/locations/1", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /locations - Success")
    public void postSuccess() throws Exception {
        Location postLocation = new Location(13.0, 12.0, new Timestamp(1000000), "a34kAo90");
        doNothing().when(kafkaSender).sendMessage(postLocation, "");
        mockMvc.perform(post("/locations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(postLocation)))
                // Response type and content type
                .andExpect(status().isOk());
    }

	@Test
	@DisplayName("DELETE /locations/1 - Success")
	public void deleteSuccess() throws Exception {
		doNothing().when(locationService).delete(1L);

		mockMvc.perform(delete("/locations/1", 1))
				.andExpect(status().isOk());
	}

	@Test
	@DisplayName("DELETE locations/1 - Not found")
	public void deleteNotFound() throws Exception {
		doThrow(NoSuchElementException.class).when(locationService).delete(any());
		mockMvc.perform(delete("/locations/{id}", 1))
				.andExpect(status().isNotFound());
	}
}
